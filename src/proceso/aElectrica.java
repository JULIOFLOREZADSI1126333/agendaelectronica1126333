/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proceso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author hp-pc
 */
public class aElectrica extends Contacto {
    
    Contacto[] lista_contactos = new Contacto[99];
    private int contador_contactos = 0; // Contador de objetos creados. Variable muy importante.  
 
    public void Consultar(String nombre, String apellido, String direccion, int telefono, String correo) { //Consultar variables
        for (int i = 0; i < this.contador_contactos; i++) {
 
            if (nombre.equals(this.lista_contactos[i].getNombre())) {
                System.out.println("Ya existe un contacto con ese nombre");
               
            }
        }
 
    }
 
    public void Anadir(String nombre, String apellido, String direccion, int telefono, String correo) { //añadir variables
        if (contador_contactos < 99) {
            this.lista_contactos[contador_contactos] = new Contacto();
            this.lista_contactos[contador_contactos].set_nombre(nombre);
            this.lista_contactos[contador_contactos].set_apellido(apellido);
            this.lista_contactos[contador_contactos].set_direccion(direccion);
            this.lista_contactos[contador_contactos].set_telefono(telefono);
            this.lista_contactos[contador_contactos].set_correo(correo);
            this.contador_contactos++;
            Ordenar();
        } else {
            System.out.println("La agenda está llena");
        }
 
    }
 
    public void Buscar(String nombre) { //buscar variable por el nombre
        boolean encontrado = false; 
        for (int i = 0; i < contador_contactos; i++) {
            if (nombre.equals(this.lista_contactos[i].getNombre())) {
                System.out.println(this.lista_contactos[i].getNombre()+"-"+this.lista_contactos[i].getApellido()+"-"+"-"+this.lista_contactos[i].getDireccion()+"-"+"Tf:"+this.lista_contactos[i].getTelefono() +"-"+this.lista_contactos[i].getCorreo());
                encontrado = true;
            }
        }
        if (!encontrado) {
            System.out.println("Contacto inexistente");
        }
    }
 
    public void Ordenar() {
        //Este método ordenará el array de contacos por el nombre mediante el Método Burbuja
        int N = this.contador_contactos;
        String nombre1;
        String nombre2;
        //Optimizo para cuando tenga más de dos elementos al menos.
        if (contador_contactos >= 2) {
            for (int i = 1; i <= N - 1; i++) {
                for (int j = 1; j <= N - i; j++) {
                    nombre1 = this.lista_contactos[j - 1].getNombre();
                    nombre2 = this.lista_contactos[j].getNombre();
                    if (nombre1.charAt(0) > nombre2.charAt(0)) {
                        Contacto tmp = this.lista_contactos[j - 1];
                        this.lista_contactos[j - 1] = this.lista_contactos[j];
                        this.lista_contactos[j] = tmp;
                    }
                }
            }
        }
    }
 
    
 
    public void Eliminar() { //eliminar algun contacto
        try {
            boolean encontrado = false;
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Nombre de contacto a eliminar:");
            String eliminar = teclado.readLine().toUpperCase();
            if (contador_contactos == 0) {
                System.out.println("No hay contactos");
            } else {
                for (int i = 0; i < contador_contactos; i++) {
 
                    if (eliminar.equals(this.lista_contactos[i].getNombre())) {
                        System.out.println(i + 1 + ". " + this.lista_contactos[i].getNombre() + ". " + this.lista_contactos[i].getApellido() + "-" +"-"+this.lista_contactos[i].getDireccion()+"-"+ "Tf:" + this.lista_contactos[i].getTelefono() +"-"+this.lista_contactos[i].getCorreo());
                        encontrado = true;
                    }
                }
                if (encontrado) { //si esto se encuentra 
                    System.out.println(" introduce el número asociado del contacto a eliminar");
                    int eliminar_numero = Integer.parseInt(teclado.readLine());
                    eliminar_numero--;
                    System.out.println("¿Estas seguro (S/N)?");
                    String respuesta;
                    respuesta = teclado.readLine();
                    respuesta = respuesta.toUpperCase();
                    if (respuesta.equals("S")) {
                        Contacto[] temporal = new Contacto[99];
                        int ii = 0;
                        boolean encontrado2=false;
                        for (int i = 0; i < this.contador_contactos; i++) {
 
                            if (i != eliminar_numero) {
                                // Creo el objeto temporal para el borrado
                                if (!encontrado2)
                                {
                                  temporal[ii] = this.lista_contactos[ii];
                                  ii++;
                                }
                                else
                                {
                                    if (ii<this.contador_contactos)
                                    { temporal[ii] = this.lista_contactos[ii+1];
                                     ii++;
                                    }
                                }
 
                            } else {
                                temporal[ii] = this.lista_contactos[ii + 1];
                                ii++;
                                encontrado2=true;
 
                            }
                        }
                        //mandará mensaje si el contacto ya fue eliminado
                        this.contador_contactos--;
                        System.out.println("Contacto eliminado correctamente");
                        for (int j = 0; j < this.contador_contactos; j++) {
                            this.lista_contactos[j] = temporal[j];
 
                        }
 
                    }
                    
                } else { //si no se encuentra en contacto que se quiera eliminara, dará este mensaje
                    System.out.println("Lo siento, No se ha encontrado el nombre");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(aElectrica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
    public void Modificar() { //para modificar varibles 
        try {
            boolean encontrado = false;
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Nombre de contacto a modificar:");
            String eliminar = teclado.readLine().toUpperCase();
            if (contador_contactos == 0) { 
                System.out.println("No hay contactos");
            } else {
                for (int i = 0; i < this.contador_contactos; i++) {
 
                    if (eliminar.equals(this.lista_contactos[i].getNombre())) {
                        System.out.println(i + 1 + ". " + this.lista_contactos[i].getNombre() + "-" + this.lista_contactos[i].getApellido() + "-" +"-"+this.lista_contactos[i].getDireccion()+"-"+ "Tf:" + this.lista_contactos[i].getTelefono()+"-"+this.lista_contactos[i].getCorreo());
                        encontrado = true;
                    }
                }
                if (encontrado) { //si se encuentra el contacto a modificar debe hacer lo que dice a continuacion
                    System.out.println("Introduce el número de contacto a modificar:");
                    int modificar_numero = Integer.parseInt(teclado.readLine());
                     
                    System.out.println("Introduce nombre:");
                    String nombre_nuevo = teclado.readLine();
                    System.out.println("Introduce apellido:");
                    String apellido_nuevo = teclado.readLine();
                    System.out.println("Introduce direccion:");
                    String direccion_nuevo = teclado.readLine();
                    System.out.println("Introduce teléfono, formato numerico:");
                    Integer telefono_nuevo = Integer.parseInt(teclado.readLine());
                    System.out.println("Introduce correo:");
                    String correo_nuevo = teclado.readLine();
                        
                    this.lista_contactos[modificar_numero -1].set_nombre(nombre_nuevo);
                    this.lista_contactos[modificar_numero -1].set_apellido(apellido_nuevo);
                    this.lista_contactos[modificar_numero -1].set_direccion(direccion_nuevo);
                    this.lista_contactos[modificar_numero -1].set_telefono(telefono_nuevo);
                    this.lista_contactos[modificar_numero -1].set_correo(correo_nuevo);
                    
                    Ordenar();
                } else {
                    System.out.println("No hay contactos con ese nombre");
                }
 
            }
            
           
        } catch (IOException ex) {
            Logger.getLogger(aElectrica.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
         public void Caracteres(){
                for (int c=0; c <contador_contactos; c++) { 
                   System.out.println(c + 1 + ". " + this.lista_contactos[c].getNombre().length() + "-" + this.lista_contactos[c].getApellido().length() + "-" +"-"+this.lista_contactos[c].getDireccion().length()+"-"+ "Tf:" + this.lista_contactos[c].getTelefono()+"-"+this.lista_contactos[c].getCorreo().length());}
         }
         
      }
    




 

