/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proceso;

/**
 *
 * @author hp-pc
 */
public class Contacto {
    
     private String nombre;
     private String apellido;
     private String direccion;
    private int telefono;
    private String correo;
 
    public Contacto()
    {
    this.nombre=null;
    this.apellido=null;
    this.direccion=null;
    this.telefono=0;
    this.correo= null;
    }
    public Contacto(String nombre, String apellido, String direccion, int telefono, String correo) {
        this.nombre = nombre;
        this.apellido= apellido;
        this.direccion= direccion;
        this.telefono = telefono;
        this.correo= correo;
    }
    public void set_nombre(String nomb){        
        this.nombre=nomb.toUpperCase();
        
    }
    public void set_apellido(String apellido){
        this.apellido= apellido.toUpperCase();
       
    }
    public void set_direccion(String direc){
        this.direccion= direc;
    } 
    public void set_telefono(int telf){
        this.telefono=telf;
    }
     public void set_correo(String correo){
        this.correo=correo;
    }
    
    public String getNombre() {
        return this.nombre;
    }
    public String getApellido() {
        return this.apellido;
    }
     public String getDireccion() {
        return this.direccion;
    } 
    public int getTelefono() {
        return telefono;
    }
    public String getCorreo() {
        return this.correo;
    }    
    
}

