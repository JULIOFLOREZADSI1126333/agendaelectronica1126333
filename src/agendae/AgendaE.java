/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package agendae;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import proceso.Contacto;
import proceso.aElectrica;
 
/**
 *
 * @author hp-pc
 */
public class AgendaE extends Contacto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        try {
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            String texto="";
            char opcion='1';
            aElectrica mi_agenda= new aElectrica();
            while ((opcion=='1') || (opcion=='2') || (opcion=='3') || (opcion=='4')|| (opcion=='5') || (opcion=='6') )
            {
                System.out.println("**********************");
                System.out.println("AGENDA");
                System.out.println("**********************");
                System.out.println("1-Nuevo contacto");
                System.out.println("2-Buscar contacto");
                System.out.println("3-Modificar contacto");
                System.out.println("4-Eliminar contacto");
                System.out.println("5-Ver caracteres");
                System.out.println("0-Salir");
                System.out.println("*********************");
                texto=teclado.readLine();
                opcion= texto.charAt(0);
                System.out.println("Opción: ");
                System.out.println("---------------------");
                
                switch(opcion){
                    case '1': //en el caso en el que oprima "1" lo enviará  a hacer esto 
                        String nombre;
                        String apellido;
                        String direccion;
                        String telefono;
                        String correo;
                        boolean validar;
                        System.out.println("Por favor introduzca el nombre:");
                        nombre=teclado.readLine();
                        System.out.println("Por favor introduzca el apellido:");
                        apellido=teclado.readLine();
                        System.out.println("Por favor introduzca direccion:");
                        direccion=teclado.readLine();
                        System.out.println("Por favor introduzca el teléfono(numero):");
                        telefono=teclado.readLine();
                        System.out.println("Por favor introduzca el correo:");
                        correo=teclado.readLine();
                        validar=esNumerica(telefono);
                        if (validar){ //Si los datos diligenciados se escribieron correctamente para a validarlos
                             int telefono_entero= Integer.parseInt(telefono);
                             mi_agenda.Consultar(nombre,apellido, direccion, telefono_entero, correo);
                             mi_agenda.Anadir(nombre, apellido, direccion, telefono_entero, correo);
                        }
                        else{ //si no escribe el telefono bien le enviara el siguiente mensaje
                             System.out.println("No es un número, formato de telefono incorrecto.");}
                        
                       
                        break; //sirve para cerrar un caso
                       
                                
                    case '2':  //en el caso en el que oprima "2" lo enviará  a hacer esto 
                        System.out.println("Nombre a buscar:");
                        nombre=teclado.readLine().toUpperCase();                        
                        mi_agenda.Buscar(nombre);
                        
                        break;
                    case '3':  //en el caso en el que oprima "1" lo enviará  a modificar 
                        mi_agenda.Modificar();
                         break;
                    case '4':  //en el caso en el que oprima "4" lo enviará  a eliminar 
                        mi_agenda.Eliminar();
                        break;
                        
                   case '5':  //en el caso en el que oprima "5" lo enviará  a la cantidad de caracteres 
                        mi_agenda.Caracteres();
                        break;
                   
                    case '0':  //en el caso en el que oprima "0" lo sacará del programa 
                        System.out.println("Ha salido del programa");
                        break;
                     
                    default:  
                        System.out.println("Opción incorrecta ...");
                        opcion='1';
                     
                }
                
                
            }    
            
                    } catch (IOException ex) { //En vez de un mensaje propio se puede imprimir el objeto ex de la clase NumberFormatException
            Logger.getLogger(aElectrica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static boolean esNumerica(String str)
{
    for (char c : str.toCharArray())
    {
        if (!Character.isDigit(c)) return false;
    }
    return true;
}
}
    
    

